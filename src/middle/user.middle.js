// const userControl = require('../controllers/user.controller')
// const validate = require('../controllers/dataVerify');
// const db = require('../config/conexion.config');

import { validateUserData } from '../controllers/dataVerify.js';
import {
    addAddress, adminDeleteUser, ban, changePassword, changeRights, createUser, dataUser, deleteAddress, deleteUser,
    generateSeissionID,
    logOutUser, modifyAddress, modifyAUser, showAllAddress, usersName, validateAdmin, validateUser
} from '../controllers/user.controller.js'
import { User } from '../config/conexion.config.js';



//Show a collection from the Data Base
const showDB = (req, res, bd) => {

    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        bd.find()
            .then(resolve => res.status(200).send(resolve))
            .catch(err => res.status(404).send(err));
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Show one element of the Data Base (use ID)
const showOneDB = (req, res, bd, id) => {
    bd.find({ _id: id })
        .then(resolve => res.status(200).send(resolve))
        .catch(err => res.status(404).send('No encontrado'));
}




//Create a new user
const newUser = (req, res) => {

    let user = validateUserData(req.body, '', 'new')

    if (user.boolean == true) {
        createUser(user.data)
            .then(resolve => res.status(200).json({ status: 200 }))
            .catch(err => res.status(409).json({ msj: 'EL CORREO ELECTRONICO O NOMBRE USUARIO YA ESTA EN USO' }));
    } else {
        res.status(400).send({ msj: 'HAY INFORMACION INVALIDA EN UNO DE LOS CAMPOS' });
    }
}



//Login a new user and create a new user token
const logIn = async (req, res) => {


    let user = validateUserData(req.body, '', 'login')
    if (user.boolean == true) {
        let userAuth;
        userAuth = await validateUser(req.body);
        if (userAuth.boolean != false) {
            res.status(200).json({ token: userAuth.token, sessionID: userAuth.sessionID, userImg: userAuth.userImg, status: 200 });
        } else {
            res.status(403).json({ msj: 'Las credenciales ingresadas no corresponden a un usuario con acceso' })
        }
    } else {
        res.status(400).json({ msj: 'Lo siento.Has enviado datos invalidos.' });
    }
}



const validateToken = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        res.status(200).json({ status: 200 });

    } else {
        res.status(403).json({ status: 403 });
    }
}



//Logout an user
const logOut = async (req, res) => {


    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let response = await logOutUser(desToken._id);
        res.status(response.status).send(response.txt)

    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Show all connected users
const showUsers = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        if (desToken.range == 'admin') {
            User.find({ state: true })
                .then(resolve => res.status(200).send(usersName(resolve)))
                .catch(err => res.status(404).send(err));
        } else {
            res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Show user data
const showDataUser = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let response = await dataUser(desToken._id);
        res.status(response.status).send(response.txt)
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Modify a user
const modUser = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user = validateUserData(req.body, '', 'update')
        let validator = user.phone || user.username || user.name || user.email || user.address || userImg;
        if (validator != false) {
            let response = await modifyAUser(user, desToken._id);
            res.status(response.status).send({ msj: response.txt, status: response.status })
        } else {

            res.status(400).json({ msj: 'Lo siento.Has enviado datos invalidos.', status: 400 });
        }
    } else {
        res.status(403).send({ msj: 'Token invalido o expirado', status: 403 });
    }
}

//Modify the password of a user
const modPassword = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);
    if (desToken != false) {
        let response = await changePassword(desToken._id, { oldPassword: req.body.oldPassword, newPassword: req.body.newPassword});
        res.status(response.status).json({ msj: response.txt, status: response.status })
    } else {
        res.status(403).json({ msj: 'Token invalido o expirado', status: 403 });
    }
}

//Modify the rights of a user
const modRights = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);

    let newRange = '';

    (req.query.rights == 'admin' || req.query.rights == 'client')
        ? (newRange = req.query.rights)
        : (newRange = undefined)

    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.name != false && newRange != undefined) {
            if (desToken.range == 'admin') {
                let response = await changeRights(user.name, newRange);
                res.status(response.status).send(response.txt);
            } else {
                res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
            }
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//Delete a user
const delUser = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
            let response = await deleteUser(desToken._id);
            res.status(response.status).json({msj : response.txt , status : response.status});
    } else {
        res.status(403).json({msj : 'Token invalido o expirado' , status : 403});
    }
}


//Show all addresses
const showAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let response = await showAllAddress(desToken._id);
        res.status(response.status).send(response.txt);
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}



//Add a new address
const newAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
            let response = await addAddress(desToken._id, req.body.address);
            res.status(response.status).json({msj : response.txt,status :  response.status});
    } else {
        res.status(403).json( {msj : 'Token invalido o expirado',status : 403});
    }
}

//Modify an address
const modAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.address != false && user.oldAdr != false) {
            let response = await modifyAddress(desToken._id, user.oldAdr, user.address);
            res.status(response.status).send(response.txt);
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Delete an address
const delAddress = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);


    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.oldAdr !== false) {
            let response = await deleteAddress(desToken._id, user.oldAdr);
            res.status(response.status).json({ msj: response.txt, status: response.status });
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}


//Ban or unban a user
const usersBan = async (req, res) => {

    let desToken = validateAdmin(req.headers.authorization);
    let banReq = '';


    (req.query.ban == 'banear usuario' || req.query.ban == 'desbanear usuario')
        ? ('')
        : (banReq = undefined)



    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.name != false && banReq != undefined) {
            if (desToken.range == 'admin') {
                (req.query.ban == 'banear usuario') ? (banReq = true) : (banReq = false)
                let response = await ban(user.name, banReq);
                res.status(response.status).send(response.txt);
            } else {
                res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
            }
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

//An admin delete a user
const delUserAdmin = async (req, res) => {
    let desToken = validateAdmin(req.headers.authorization);

    if (desToken != false) {
        let user;
        user = validateUserData(req.body, '', 'update');
        if (user.name != false) {
            if (desToken.range == 'admin') {
                let response = await adminDeleteUser(user.name);
                res.status(response.status).send(response.txt);
            } else {
                res.status(403).send('Lo siento, no tienes permiso para acceder a este contenido.');
            }
        } else {
            res.status(400).send('Lo siento.Has enviado datos invalidos.');
        }
    } else {
        res.status(403).send('Token invalido o expirado');
    }
}

export {
    showDB,
    showUsers,
    showOneDB,
    showDataUser,
    newUser,
    logIn,
    logOut,
    modUser,
    modRights,
    delUser,
    showAddress,
    newAddress,
    modAddress,
    delAddress,
    usersBan,
    validateToken,
    delUserAdmin,
    modPassword
}