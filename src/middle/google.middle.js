import { loginGoogle } from "../controllers/google.controller.js";

const loginWithGoogle = (req, res) => {
    loginGoogle({
        googleID: req.user.id,
        type: 1,
        name: `${req.user.name.givenName}${(req.user.name.familyName == undefined) ? ('') : (` ${req.user.name.familyName}`)}`,
        email: req.user.email,
        userImg: req.user.picture
    })
        .then(resolve => res.status(200).cookie('userImg', (resolve.userImg),{ maxAge: 1 * 60 * 1000})
        .cookie('token', resolve.token,{ maxAge: 1 * 60 * 1000})
        .cookie('sessionID', resolve.sessionID,{ maxAge: 1 * 60 * 1000})
        .redirect(`http://localhost:3001/testenado/token:${resolve}`))
        .catch(err => res.status(400).json({ msj: err }));
}

export {
    loginWithGoogle
}