const assert = require('assert');
const fetch = require('node-fetch');
const url = `http://localhost:3000/user/signUp`;

let userTest = {
    username: 'Maria',
    name: 'Maria Santillan',
    phone: 12123145,
    email: 'maria@gmail.com',
    address: 'Cordoba',
    password: '1234'
}



describe(`Test de Registro de Usuarios`, () => {
    it(`Usuario registrado`, async () => {
        await fetch(url, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(userTest)
        })
            .then(response => {response.json()
                assert.equal(response.status, 200);
            })
    });
})