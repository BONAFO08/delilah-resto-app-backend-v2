import 'dotenv/config.js'
import  express  from "express";
const router = express.Router();
import passport from 'passport'
import GoogleStrategy from 'passport-google-oauth2';
import { loginWithGoogle } from '../middle/google.middle.js';

let docum = [];

let url_frontEnd = 'http://localhost:3001';

passport.serializeUser((user, cb) => {
    cb(null, user)
})

passport.deserializeUser((user, cb) => {
    cb(null, user)
})


passport.use(new GoogleStrategy({
    clientID: "872107663187-uh3nu996f0eqbj9tkra7s9kbrub9npug.apps.googleusercontent.com",
    clientSecret: "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",
    callbackURL: "http://localhost:3000/auth/google/callback",
    passReqToCallback: true,
    scope: ['email', 'profile']
},
    function (request, accessToken, refreshToken, profile, done) {
        return done(null, profile);
    }
));


router.get('/auth/google/failure', (req, res) => {
    res.send('fail')
});

router.get('/auth/google',
    passport.authenticate('google', {
        scope:
            ['email', 'profile']
    }
    ));

router.get('/auth/google/callback',
    passport.authenticate('google', {
        failureRedirect: '/auth/google/failure'
    }), (req, res) => {
        // let aux = docum.filter(arr => arr.id == req.user.id);
        // res.redirect(`${url_frontEnd}/google/${req.user.id}`)

        loginWithGoogle(req,res)
    });








export {router}

