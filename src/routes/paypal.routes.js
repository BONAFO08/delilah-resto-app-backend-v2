import 'dotenv/config.js'
import express from "express";
import paypal from "paypal-rest-sdk"
import { createOrder } from "../controllers/order.controller.js";
import { consumeToken } from "../controllers/user.controller.js";
const router = express.Router();
const dolar = 115.19;

let ordersNoConfirms = [];
paypal.configure({
    'mode': 'sandbox',
    'client_id': process.env.PAYPAL_ID,
    'client_secret': process.env.PAYPAL_SECRET
});

router.post('/cosa', (req, res) => {
    const desToken = consumeToken(req.headers.authorization);
    req.body.order.token = desToken;
    ordersNoConfirms.push(req.body.order)
    res.redirect("/paypal")
})

router.post('/paypal', (req, res) => {
    const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];
    if (order == undefined) {
        res.redirect("http://localhost:3000/cancel")
    }



    const create_payment_json = {
        "intent": "sale",
        "payer": {
            "payment_method": "paypal"
        },
        "redirect_urls": {
            "return_url": `http://localhost:3000/success?orderID=${req.query.orderID}`,
            "cancel_url": `http://localhost:3000/cancel?orderID=${req.query.orderID}`
        },
        "transactions": [{
            "item_list": {
                "items": order.arrFood
            },
            "amount": {
                "currency": "USD",
                "total": order.totalAmmout
            },
            "description": "Hat for the best team ever"
        }]
    };

    router.get('/success', (req, res) => {
        const payerId = req.query.PayerID;
        const paymentId = req.query.paymentId;
        const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];

        const execute_payment_json = {
            "payer_id": payerId,
            "transactions": [{
                "amount": {
                    "currency": "USD",
                    "total": order.totalAmmout
                }
            }]
        };

        paypal.payment.execute(paymentId, execute_payment_json, function (error, payment) {
            if (error) {
                console.log(error.response);
                throw error;
            } else {
                const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];
                ordersNoConfirms = ordersNoConfirms.filter(order => order.orderID != req.query.orderID);
       
                createOrder(order.token, order.address, order.idsProducts, order.idPay, payment.id)
                    .then(resolve => res.send(resolve))
                    .catch(error => res.send(error));

                    res.redirect('http://localhost:3001/sucessPaypal')
            }
        });
    });

    paypal.payment.create(create_payment_json, function (error, payment) {
        if (error) {
            throw error;
        } else {
            for (let i = 0; i < payment.links.length; i++) {
                if (payment.links[i].rel === 'approval_url') {
                    res.redirect(payment.links[i].href);
                }
            }
        }
    });

});

router.get('/cancel', (req, res) => {
    const order = ordersNoConfirms.filter(order => order.orderID == req.query.orderID)[0];
    ordersNoConfirms = ordersNoConfirms.filter(order => order.orderID != req.query.orderID);
    console.log(ordersNoConfirms);
    res.redirect('http://localhost:3001/cancelPaypal')
});







export { router }
