import  express  from "express";
import { cancelOrder, changeStadeOrder, confOrder, modOrder, newOrder, showAllOrders, showUserOrders } from '../middle/order.middle.js'
const router = express.Router();


/**
 * @swagger
 * /showUserOrders:
 *  get:
 *    tags: 
 *      [Order]
 *    summary: Mostrar pedidos de usuario
 *    description: Muestra todos los pedidos de un usuario
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Se muestran todos los pedidos del usuario
 *            403:
 *                description: Credenciales incorrectas
 */


 router.get('/showUserOrders', (req, res) => {
    showUserOrders(req, res);
});

/**
 * @swagger
 * /showAllOrders:
 *  get:
 *    tags: 
 *      [Order]
 *    summary: Mostrar pedidos
 *    description: Muestra todos los pedidos
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    responses:
 *            200:
 *                description: Se muestran todos los pedidos
 *            403:
 *                description: Credenciales incorrectas
 */


router.get('/showAllOrders', (req, res) => {
    showAllOrders(req, res);
});

/**
 * @swagger
 * /newOrder/:
 *  post:
 *    tags: 
 *      [Order]
 *    summary: Crear Pedido
 *    description: Crea un pedido y lo almacena en la Base de Datos
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: idPay
 *      description: Id del medio de pago
 *      in: query
 *      required: false
 *      type: string
 *    - name: address
 *      description: Numeración del domicilio  dentro de la agenda del usuario  (Num Min  1)
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: arrFood
 *      description: Comida seleccionada [Requiere Id del producto y ammount para la cantidad de este]
 *      in: formData
 *      required: false
 *      type: array
 *      items: 
 *        type: object
 *        properties: 
 *          id: 
 *             type: string
 *          ammount: 
 *             type: integer
 *    responses:
 *            200:
 *                description: Pedido creado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Alguno de los items seleccionados no existe en sus respectivas colecciones
 */



router.post('/newOrder/', (req, res) => {
 newOrder(req, res);
});

/**
 * @swagger
 * /modOrder/:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Modificar datos de un Pedido
 *    description: Modifica los datos de un pedido
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: idOrder
 *      description: Id del pedido
 *      in: query
 *      required: false
 *      type: string
 *    - name: idPay
 *      description: Id del medio de pago
 *      in: query
 *      required: false
 *      type: string
 *    - name: address
 *      description: Numeración del domicilio  dentro de la agenda del usuario  (Num Min  1)
 *      in: formData
 *      required: false
 *      type: integer
 *    - name: arrFood
 *      description: Comida seleccionada [Requiere Id del producto para modificar cantidad o agregar uno nuevo y ammount para mofificar la cantidad de este o ammount = 0 para eliminarlo del pedido]
 *      in: formData
 *      required: false
 *      type: array
 *      items: 
 *        type: object
 *        properties: 
 *          id: 
 *             type: string
 *          ammount: 
 *             type: integer
 *    responses:
 *            200:
 *                description: Pedido modificado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Alguno de los items seleccionados no existe en sus respectivas colecciones
 */


router.put('/modOrder/', (req, res) => {
    modOrder(req, res);
});

/**
 * @swagger
 * /cancelOrder/:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Cancelar Pedido
 *    description: Cancela el pedido si este no ha sido entregado.
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: idOrder
 *      description: Id del pedido
 *      in: query
 *      required: false
 *      type: string
 *      items: 
 *        type: object
 *        properties: 
 *          id: 
 *             type: string
 *          ammount: 
 *             type: integer
 *    responses:
 *            200:
 *                description: Pedido Cancelado
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Pedido no encontrado
 */


router.put('/cancelOrder/', (req, res) => {
    cancelOrder(req, res);
});

/**
 * @swagger
 * /confOrder/:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Confirmar Orden
 *    description: Confirma la orden.
 *    parameters:
 *    - name: authorization
 *      description: Token de usuario
 *      in: header
 *      required: false
 *      type: string
 *    - name: idOrder
 *      description: Id del pedido
 *      in: query
 *      required: false
 *      type: string
 *      items: 
 *        type: object
 *        properties: 
 *          id: 
 *             type: string
 *          ammount: 
 *             type: integer
 *    responses:
 *            200:
 *                description: Pedido Confirmado
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Pedido no encontrado
 */


router.put('/confOrder/', (req, res) => {
    confOrder(req, res);
});

/**
 * @swagger
 * /changeStadeOrder/:
 *  put:
 *    tags: 
 *      [Order]
 *    summary: Cambiar estado de un Pedido
 *    description: Cambia el estado de un pedido.
 *    parameters:
 *    - name: authorization
 *      description: Token de admin
 *      in: header
 *      required: false
 *      type: string
 *    - name: idOrder
 *      description: Id del pedido
 *      in: query
 *      required: false
 *      type: string
 *    - name: newStade
 *      description: Nuevo estado del pedido
 *      in: query
 *      type: string
 *      enum: [Confirmado,En preparación,Enviado,Entregado,Cancelado]
 *      required: false
 *      items: 
 *        type: object
 *        properties: 
 *          id: 
 *             type: string
 *          ammount: 
 *             type: integer
 *    responses:
 *            200:
 *                description: Medio de pago creado exitosamente
 *            403:
 *                description: Credenciales incorrectas
 *            400:
 *                description: Error al validar los datos 
 *            404:
 *                description: Pedido no encontrado
 */


router.put('/changeStadeOrder/', (req, res) => {
    changeStadeOrder(req, res);
});


export {router}
