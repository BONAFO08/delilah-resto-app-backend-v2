_**DELILAH RESTO MANUAL DE USUARIO**_


FRONTEND: https://gitlab.com/BONAFO08/delilah-resto-frontend-vanilla

BACKEND: https://gitlab.com/BONAFO08/delilah-resto-app-backend-v2

Version actual del Proyecto: 

FRONTEND: Alfa 1.0 

BACKEND: 4.0 

Nota importante: La interfaz (Frontend) aún no está terminada. Las siguientes funciones están actualmente implementadas:

-Registro de usuario
-Login de usuario (Google y Local)
-Visualización de productos
-Visualización de carrito
-Visualización de la información de la cuenta del usuario 
-Modificación de la información de la cuenta del usuario
-Creación de un pedido
-Confirmación de un pedido (Paypal y Local)
-Eliminación de la cuenta.
